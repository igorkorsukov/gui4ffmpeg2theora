#ifndef HELPWINDOW_H
#define HELPWINDOW_H

#include <QtCore/QDebug>
#include <QtGui/QWidget>
#include <QtGui/QTextBrowser>
#include <QtGui/QPushButton>
#include <QtGui/QBoxLayout>
#include <QtGui/QPrinter>
#include <QtGui/QPrintDialog>


class QUHelpBrowser: public QWidget
{
Q_OBJECT
public:
    QUHelpBrowser(QString wtitle, QIcon wicon);

signals:

public slots:


private slots:
    void printDialog();
    void setWindowTitleFromDoc();

private:
    QString windowTitle;

    QBoxLayout *mainLayout;
        QBoxLayout *btnLayout;
            QPushButton *homeButton;
            QPushButton *backButton;
            QPushButton *forwardButton;
            QPushButton *printButton;

        QTextBrowser *textBrowser;


};

#endif // HELPWINDOW_H
