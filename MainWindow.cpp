#include "MainWindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    findex = 0;
    isStartConvert = false;
    lastConvertedFile = -1;
    progress = 0;

    settings = new QSettings("GUI4ffmpeg2theora.conf", QSettings::IniFormat);

    settings->beginGroup("outputpaths");
        pathsList = new QStringList(settings->value("paths").toStringList());
    settings->endGroup();

    setWindowIcon(QIcon(":/icons/fish16.png"));
    setWindowTitle("GUI4ffmpeg2theora");

    createToolBar();
    createMainUI();

    timer = new QTimer;
    connect(timer, SIGNAL(timeout()), this, SLOT(timerAction()));

    helpBrowser = new QUHelpBrowser("GUI4ffmpeg2theora", QIcon(":/icons/help.png"));


    //infoFrame = new QFrame;
    infoMsgBox = new QTextEdit;


    vqTypeChange();
    aqTypeChange();


    //coderproc = new QUCoderProc();
    coderproc = new QProcess();
    //coderproc->setOpenMode(QIODevice::Unbuffered);
    coderproc->setEnvironment(QProcess::systemEnvironment());
    connect(coderproc, SIGNAL(finished(int)), this, SLOT(nextConvert(int)));
    //connect(coderproc, SIGNAL(readyReadStandardOutput()), this, SLOT(ffmpeg2theoraMsg()));
    //connect(coderproc, SIGNAL(readyReadStandardError()), this, SLOT(ffmpeg2theoraMsg()));
    coderproc->setProcessChannelMode(QProcess::MergedChannels);
    connect(coderproc, SIGNAL(readyRead()), this, SLOT(ffmpeg2theoraMsg()));

    coderinfo = new QProcess();
    coderinfo->setProcessEnvironment(QProcessEnvironment::systemEnvironment());
    connect(coderinfo, SIGNAL(readyReadStandardOutput()), this, SLOT(infoMsg()));
    connect(coderinfo, SIGNAL(readyReadStandardError()), this, SLOT(infoMsg()));




}

MainWindow::~MainWindow(){
    coderproc->kill();
    delete settings;
QSettings *set = new QSettings("GUI4ffmpeg2theora.conf", QSettings::IniFormat);
    set->beginGroup("outputpaths");
    //set->setValue("key", "value");
    set->setValue("paths", *pathsList);
    set->endGroup();
    delete set;
}

void MainWindow::createMainUI(){

    QFrame *mainFrame = new QFrame;
    QBoxLayout *topLayout = new QBoxLayout(QBoxLayout::TopToBottom);
    topLayout->setSpacing(2);
    topLayout->setMargin(2);

//Files
    QFrame *filesFrame = new QFrame;{
    QBoxLayout *filesLayout = new QBoxLayout(QBoxLayout::TopToBottom);
    filesLayout->setSpacing(0);
    filesLayout->setMargin(0);
        filesTable = new QTableWidget;
        filesTable->setEditTriggers(0);
        filesTable->setAlternatingRowColors(true);
        filesTable->setColumnCount(2);
        filesAllCheck = new QCheckBox(" ");
        filesAllCheck->setToolTip(tr("Check all files"));
        connect(filesAllCheck, SIGNAL(clicked()), this, SLOT(checkAllFiles()));
        //filesAllCheck->setMinimumHeight(12);
        QBoxLayout *headerLayout = new QBoxLayout(QBoxLayout::LeftToRight);
        headerLayout->addWidget(filesAllCheck);
        headerLayout->addStretch(1);
        //headerLayout->setSpacing(8);
        headerLayout->setMargin(4);
        //headerLayout->setGeometry(QRect(0, 0, 20, 20));

        QTableWidgetItem *header1 = new QTableWidgetItem;
        header1->setText("");
        QTableWidgetItem *header2 = new QTableWidgetItem;
        header2->setText(tr("Caption"));

        filesTable->setHorizontalHeaderItem(0, header1);
        filesTable->setHorizontalHeaderItem(1, header2);

        filesTable->horizontalHeader()->setLayout(headerLayout);
        filesTable->horizontalHeader()->setDefaultSectionSize(27);
        //filesTable->horizontalHeader()->setMinimumHeight(10);
        filesTable->horizontalHeader()->setStretchLastSection(true);



    filesLayout->addWidget(filesTable);
    filesFrame->setLayout(filesLayout);}

//Options

    optionsTabs = new QTabWidget;{
    QFrame *optQualityFrame = new QFrame;{
    QBoxLayout *optQualityLayout = new QBoxLayout(QBoxLayout::TopToBottom);
    optQualityLayout->setSpacing(2);
    optQualityLayout->setMargin(2);
        QGroupBox *optVQBox = new QGroupBox(tr("Video quality"));{
        QGridLayout *optVQLayout = new QGridLayout;
        optVQLayout->setSpacing(2);
        optVQLayout->setMargin(2);
            novideo = new QCheckBox(tr("Disable video from input"));
            connect(novideo, SIGNAL(clicked()), this, SLOT(novideoCheck()));
            vqType = new QComboBox;
            vqType->addItem(tr("quality"), 0);
            vqType->addItem(tr("bitrate"), 1);
            vqType->setCurrentIndex(0);
            connect(vqType, SIGNAL(currentIndexChanged(int)), this, SLOT(vqTypeChange()));
            vqDemLabel = new QLabel(tr("Quality:"));
            vqQuValue = new QSlider;
            vqQuValue->setOrientation(Qt::Horizontal);
            vqQuValue->setTickPosition(QSlider::TicksBothSides);
            vqQuValue->setMinimum(0);
            vqQuValue->setMaximum(10);
            vqQuValue->setTickInterval(1);
            vqQuValue->setValue(6);
            vqBrValue = new QSlider;
            vqBrValue->setOrientation(Qt::Horizontal);
            vqBrValue->setTickPosition(QSlider::TicksBothSides);
            vqBrValue->setMinimum(0);
            vqBrValue->setMaximum(4000);
            vqBrValue->setTickInterval(100);
            vqBrValue->setValue(920);
            connect(vqQuValue, SIGNAL(valueChanged(int)), this, SLOT(vqValueChange()));
            connect(vqBrValue, SIGNAL(valueChanged(int)), this, SLOT(vqValueChange()));
            vqValLable = new QLabel;
            vqValLable->setFrameShape(QFrame::Box);
            softtarget = new QCheckBox(tr("Soft target"));
            speedlevel[0] = new QRadioButton(tr("Slowest (best)"));
            speedlevel[1] = new QRadioButton(tr("Enable early skip (default)"));
            speedlevel[1]->setChecked(true);
            speedlevel[2] = new QRadioButton(tr("Disable motion compensation"));

        optVQLayout->addWidget(novideo, 0, 0, 1, 3);
        optVQLayout->addWidget(new QLabel(tr("Mode:")), 1, 0);
        optVQLayout->addWidget(vqType, 1, 1, 1, 2);
        optVQLayout->addWidget(vqDemLabel, 2, 0);
        optVQLayout->addWidget(vqQuValue, 2, 1);
        optVQLayout->addWidget(vqBrValue, 2, 1);
        optVQLayout->addWidget(vqValLable, 2, 2);
        optVQLayout->addWidget(softtarget, 3, 0, 1, 3);
        optVQLayout->addWidget(new QLabel(tr("Speed level:")), 4, 0, 1, 3);
        optVQLayout->addWidget(speedlevel[0], 5, 0, 1, 3);
        optVQLayout->addWidget(speedlevel[1], 6, 0, 1, 3);
        optVQLayout->addWidget(speedlevel[2], 7, 0, 1, 3);

        optVQBox->setLayout(optVQLayout);}

        QGroupBox *optAQBox = new QGroupBox(tr("Audio quality"));{
        QGridLayout *optAQLayout = new QGridLayout;
        optAQLayout->setSpacing(2);
        optAQLayout->setMargin(2);
            noaudio = new QCheckBox(tr("Disable audio from input"));
            connect(noaudio, SIGNAL(clicked()), this, SLOT(noaudioCheck()));
            aqType = new QComboBox;
            aqType->addItem(tr("quality"), 0);
            aqType->addItem(tr("bitrate"), 1);
            connect(aqType, SIGNAL(currentIndexChanged(int)), this, SLOT(aqTypeChange()));
            aqDemLabel = new QLabel(tr("Quality:"));
            aqQuValue = new QSlider;
            aqQuValue->setOrientation(Qt::Horizontal);
            aqQuValue->setTickPosition(QSlider::TicksBothSides);
            aqQuValue->setMinimum(0);
            aqQuValue->setMaximum(10);
            aqQuValue->setTickInterval(1);
            aqQuValue->setValue(6);
            aqBrValue = new QSlider;
            aqBrValue->setOrientation(Qt::Horizontal);
            aqBrValue->setTickPosition(QSlider::TicksBothSides);
            aqBrValue->setMinimum(32);
            aqBrValue->setMaximum(500);
            aqBrValue->setTickInterval(50);
            aqBrValue->setValue(128);
            connect(aqQuValue, SIGNAL(valueChanged(int)), this, SLOT(aqValueChange()));
            connect(aqBrValue, SIGNAL(valueChanged(int)), this, SLOT(aqValueChange()));
            aqValLable = new QLabel;
            aqValLable->setFrameShape(QFrame::Box);

            aqChNumMode[0] = new QRadioButton(tr("Current"));
            aqChNumMode[0]->setChecked(true);
            aqChNumMode[1] = new QRadioButton(tr("Custom"));
            connect(aqChNumMode[0], SIGNAL(clicked()), this, SLOT(aqChNumModeChange()));
            connect(aqChNumMode[1], SIGNAL(clicked()), this, SLOT(aqChNumModeChange()));
            aqChNumbers = new QSpinBox;
            aqChNumbers->setMaximumWidth(45);
            aqChNumbers->setMaximum(255);
            aqChNumbers->setValue(2);
            aqChNumbers->setEnabled(false);
            QBoxLayout *aqChNumLayout = new QBoxLayout(QBoxLayout::LeftToRight);
            aqChNumLayout->addWidget(aqChNumMode[1]);
            aqChNumLayout->addWidget(aqChNumbers);
            QButtonGroup *aqChNumGroup = new QButtonGroup;
            aqChNumGroup->addButton(aqChNumMode[0]);
            aqChNumGroup->addButton(aqChNumMode[1]);

            aqSamplerateMode[0] = new QRadioButton(tr("Current"));
            aqSamplerateMode[0]->setChecked(true);
            aqSamplerateMode[1] = new QRadioButton(tr("Custom"));
            connect(aqSamplerateMode[0], SIGNAL(clicked()), this, SLOT(aqSrModeChange()));
            connect(aqSamplerateMode[1], SIGNAL(clicked()), this, SLOT(aqSrModeChange()));
            aqSamplerate = new QSpinBox;
            aqSamplerate->setMaximumWidth(55);
            aqSamplerate->setMaximum(96000);
            aqSamplerate->setValue(44100);
            aqSamplerate->setEnabled(false);
            QBoxLayout *aqSamRtLayout = new QBoxLayout(QBoxLayout::LeftToRight);
            aqSamRtLayout->addWidget(aqSamplerateMode[1]);
            aqSamRtLayout->addWidget(aqSamplerate);
            QButtonGroup *aqSamRtGroup = new QButtonGroup;
            aqSamRtGroup->addButton(aqSamplerateMode[0]);
            aqSamRtGroup->addButton(aqSamplerateMode[1]);

        optAQLayout->addWidget(noaudio, 0, 0, 1, 3);
        optAQLayout->addWidget(new QLabel("Mode:"), 1, 0);
        optAQLayout->addWidget(aqType, 1, 1, 1, 2);
        optAQLayout->addWidget(aqDemLabel, 2, 0);
        optAQLayout->addWidget(aqQuValue, 2, 1);
        optAQLayout->addWidget(aqBrValue, 2, 1);
        optAQLayout->addWidget(aqValLable, 2, 2);
        optAQLayout->addWidget(new QLabel(tr("Set number of output channels:")), 3, 0, 1, 3);
        optAQLayout->addWidget(aqChNumMode[0], 4, 0, 1, 3);
        optAQLayout->addLayout(aqChNumLayout, 5, 0, 1, 3);
        optAQLayout->addWidget(new QLabel(tr("Set output samplerate (in Hz):")), 6, 0, 1, 3);
        optAQLayout->addWidget(aqSamplerateMode[0], 7, 0, 1, 3);
        optAQLayout->addLayout(aqSamRtLayout, 8, 0, 1, 3);



        optAQBox->setLayout(optAQLayout);}

    optQualityLayout->addWidget(optVQBox);
    optQualityLayout->addWidget(optAQBox);
    optQualityLayout->addStretch(1);

    optQualityFrame->setLayout(optQualityLayout);}

    QFrame *optMetaFrame = new QFrame;{
    QBoxLayout *optMetadataLayout = new QBoxLayout(QBoxLayout::TopToBottom);
    optMetadataLayout->setSpacing(2);
    optMetadataLayout->setMargin(2);

        //QGroupBox *optMetaBox = new QGroupBox(tr("Metadata"));
        QGridLayout *optMetaLayout = new QGridLayout;
        optMetaLayout->setSpacing(2);
        optMetaLayout->setMargin(2);
            artist = new QLineEdit;
            title = new QLineEdit;
            date = new QLineEdit;;
            location = new QLineEdit;
            organization = new QLineEdit;
            copyright = new QLineEdit;
            license = new QLineEdit;
            contact = new QLineEdit;

            nometadata = new QCheckBox(tr("Disables metadata from input"));
            nooshash = new QCheckBox(tr("Do not include oshash of source file"));

        optMetaLayout->addWidget(new QLabel(tr("Artist:")), 0, 0);
        optMetaLayout->addWidget(artist, 0, 1);
        optMetaLayout->addWidget(new QLabel(tr("Title:")), 1, 0);
        optMetaLayout->addWidget(title, 1, 1);
        optMetaLayout->addWidget(new QLabel(tr("Date:")), 2, 0);
        optMetaLayout->addWidget(date, 2, 1);
        optMetaLayout->addWidget(new QLabel(tr("Location:")), 3, 0);
        optMetaLayout->addWidget(location, 3, 1);
        optMetaLayout->addWidget(new QLabel(tr("Organization:")), 4, 0);
        optMetaLayout->addWidget(organization, 4, 1);
        optMetaLayout->addWidget(new QLabel(tr("Copyright:")), 5, 0);
        optMetaLayout->addWidget(copyright, 5, 1);
        optMetaLayout->addWidget(new QLabel(tr("License:")), 6, 0);
        optMetaLayout->addWidget(license, 6, 1);
        optMetaLayout->addWidget(new QLabel(tr("Contact:")), 7, 0);
        optMetaLayout->addWidget(contact, 7, 1);
        optMetaLayout->addWidget(nometadata, 8, 0, 1, 2);
        optMetaLayout->addWidget(nooshash, 9, 0, 1, 2);

        //optMetaBox->setLayout(optMetaLayout);}

     //optMetadataLayout->addWidget(optMetaBox);
    optMetadataLayout->addLayout(optMetaLayout);
    optMetadataLayout->addStretch(1);
    optMetaFrame->setLayout(optMetadataLayout);}

    QFrame *optSuptitlesFrame = new QFrame;{
        QBoxLayout *optSuptitlesLayout = new QBoxLayout(QBoxLayout::TopToBottom);
        optSuptitlesLayout->setSpacing(2);
        optSuptitlesLayout->setMargin(2);
        QBoxLayout *stfileLayout = new QBoxLayout(QBoxLayout::LeftToRight);
            stFilePath = new QLineEdit;
            QPushButton *btnSFileBrowser = new QPushButton(tr("..."));
            btnSFileBrowser->setMaximumWidth(30);
        stfileLayout->addWidget(stFilePath, 1);
        stfileLayout->addWidget(btnSFileBrowser);

        QGridLayout *stOptLayout = new QGridLayout;
            stEncoding = new QComboBox;
            stEncoding->addItem(tr("utf-8"));
            stEncoding->addItem(tr("utf8"));
            stEncoding->addItem(tr("iso-8859-1"));
            stEncoding->addItem(tr("latin1"));

            stLang = new QLineEdit;

            stCategory = new QLineEdit;
            stCategory->setText("subtitles");

            ignoreNonUtf8 = new QCheckBox(tr("ignores any non UTF-8 sequence in UTF-8 text"));
            noSubtitles = new QCheckBox(tr("disables subtitles from input"));

        stOptLayout->addWidget(new QLabel(tr("Encoding:")), 0, 0);
        stOptLayout->addWidget(stEncoding, 0, 1);
        stOptLayout->addWidget(new QLabel(tr("Language:")), 1, 0);
        stOptLayout->addWidget(stLang, 1, 1);
        stOptLayout->addWidget(new QLabel(tr("Category:")), 2, 0);
        stOptLayout->addWidget(stCategory, 2, 1);
        stOptLayout->addWidget(ignoreNonUtf8, 3, 0, 1, 2);
        stOptLayout->addWidget(noSubtitles, 4, 0, 1, 2);

        optSuptitlesLayout->addWidget(new QLabel(tr("Use subtitles from the file SubRip(.srt):")));
        optSuptitlesLayout->addLayout(stfileLayout);
        optSuptitlesLayout->addLayout(stOptLayout);
        optSuptitlesLayout->addStretch(1);
    optSuptitlesFrame->setLayout(optSuptitlesLayout);
    }
    QFrame *optPresetFrame = new QFrame;
    QFrame *optSizeFrame = new QFrame;{
        QBoxLayout *optSizeLayout = new QBoxLayout(QBoxLayout::TopToBottom);
        QGroupBox *groupSize = new QGroupBox(tr("Size"));{
            QBoxLayout *groupSizeLayout = new QBoxLayout(QBoxLayout::TopToBottom);
            sizeVideo[0] = new QRadioButton(tr("Current"));
            sizeVideo[0]->setChecked(true);
            sizeVideo[1] = new QRadioButton(tr("New"));
            connect(sizeVideo[0], SIGNAL(clicked()), this, SLOT(sizeChange()));
            connect(sizeVideo[1], SIGNAL(clicked()), this, SLOT(sizeChange()));

            QBoxLayout *sizeLayout = new QBoxLayout(QBoxLayout::LeftToRight);
            widthEdit = new QLineEdit;
            heightEdit = new QLineEdit;
            preserveAspect = new QCheckBox(tr("Preserve aspect ratio"));
            noupscaling = new QCheckBox(tr("Only scale video or resample audio if input is bigger"));

            sizeLayout->addWidget(sizeVideo[1]);
            sizeLayout->addWidget(new QLabel(tr("Width:")));
            sizeLayout->addWidget(widthEdit);
            sizeLayout->addSpacing(20);
            sizeLayout->addWidget(new QLabel(tr("Height:")));
            sizeLayout->addWidget(heightEdit);

            groupSizeLayout->addWidget(sizeVideo[0]);
            //groupSizeLayout->addWidget(sizeVideo[1]);
            groupSizeLayout->addLayout(sizeLayout);
            //groupSizeLayout->addWidget(preserveAspect);
            groupSizeLayout->addWidget(noupscaling);

            groupSize->setLayout(groupSizeLayout);
            sizeChange();
        }

        QGroupBox *groupAspect = new QGroupBox(tr("Aspect ratio"));{
        QBoxLayout *aspectLayout = new QBoxLayout(QBoxLayout::TopToBottom);
            aspectVideo[0] = new QRadioButton(tr("Current"));
            aspectVideo[0]->setChecked(true);
            aspectVideo[1] = new QRadioButton(tr("New"));
            connect(aspectVideo[0], SIGNAL(clicked()), this, SLOT(aspectChange()));
            connect(aspectVideo[1], SIGNAL(clicked()), this, SLOT(aspectChange()));

            QBoxLayout *setAspectLayout = new QBoxLayout(QBoxLayout::LeftToRight);
            aspect = new QComboBox;
            aspect->addItem(tr("frame aspect ratio 4:3"));
            aspect->addItem(tr("frame aspect ratio 16:9"));
            aspect->addItem(tr("pixel aspect ratio 1:1"));
            aspect->addItem(tr("pixel aspect ratio 4:3"));

            setAspectLayout->addWidget(aspectVideo[1]);
            setAspectLayout->addWidget(aspect);

        aspectLayout->addWidget(aspectVideo[0]);
        aspectLayout->addLayout(setAspectLayout);

        groupAspect->setLayout(aspectLayout);
        aspectChange();
        }

        QGroupBox *groupCrop = new QGroupBox(tr("Crop input by given pixels before resizing"));{
        QGridLayout *cropLayout = new QGridLayout;
        cropTop = new QLineEdit;
        cropTop->setText("0");
        cropBottom = new QLineEdit;
        cropBottom->setText("0");
        cropLeft = new QLineEdit;
        cropLeft->setText("0");
        cropRight = new QLineEdit;
        cropRight->setText("0");

        cropLayout->addWidget(new QLabel(tr("Top:")), 0, 0);
        cropLayout->addWidget(cropTop, 0, 1);
        cropLayout->addWidget(new QLabel(tr("Bottom:")), 0, 2);
        cropLayout->addWidget(cropBottom, 0, 3);
        cropLayout->addWidget(new QLabel(tr("Left:")), 1, 0);
        cropLayout->addWidget(cropLeft, 1, 1);
        cropLayout->addWidget(new QLabel(tr("Right:")), 1, 2);
        cropLayout->addWidget(cropRight, 1, 3);

        groupCrop->setLayout(cropLayout);
        }

        optSizeLayout->addWidget(groupSize);
        optSizeLayout->addWidget(groupAspect);
        optSizeLayout->addWidget(groupCrop);
        optSizeLayout->addStretch(1);

        optSizeFrame->setLayout(optSizeLayout);
    }
    QFrame *optTransferFrame = new QFrame;{
    QBoxLayout *optTransferLayout = new QBoxLayout(QBoxLayout::TopToBottom);

        QGroupBox *groupTransfar = new QGroupBox(tr("Transfer"));
        QGridLayout *transferLayout = new QGridLayout;
            contrast = new QSlider;
            contrast->setOrientation(Qt::Horizontal);
            contrast->setTickPosition(QSlider::TicksBothSides);
            contrast->setMinimum(1);
            contrast->setMaximum(100);
            contrast->setTickInterval(1);
            contrast->setValue(10);
            connect(contrast, SIGNAL(valueChanged(int)), this, SLOT(contrastChange(int)));
            contrastLable = new QLabel;
            contrastLable->setFrameShape(QFrame::Box);
            contrastLable->setMaximumWidth(28);
            contrastLable->setMinimumWidth(28);

            brightness = new QSlider;
            brightness->setOrientation(Qt::Horizontal);
            brightness->setTickPosition(QSlider::TicksBothSides);
            brightness->setMinimum(-10);
            brightness->setMaximum(10);
            brightness->setTickInterval(1);
            brightness->setValue(0);
            connect(brightness, SIGNAL(valueChanged(int)), this, SLOT(brightnessChange(int)));
            brightnessLable = new QLabel;
            brightnessLable->setFrameShape(QFrame::Box);
            brightnessLable->setMaximumWidth(28);
            brightnessLable->setMinimumWidth(28);

            gamma = new QSlider;
            gamma->setOrientation(Qt::Horizontal);
            gamma->setTickPosition(QSlider::TicksBothSides);
            gamma->setMinimum(1);
            gamma->setMaximum(100);
            gamma->setTickInterval(1);
            gamma->setValue(10);
            connect(gamma, SIGNAL(valueChanged(int)), this, SLOT(gammaChange(int)));
            gammaLable = new QLabel;
            gammaLable->setFrameShape(QFrame::Box);
            gammaLable->setMaximumWidth(28);
            gammaLable->setMinimumWidth(28);

            saturation = new QSlider;
            saturation->setOrientation(Qt::Horizontal);
            saturation->setTickPosition(QSlider::TicksBothSides);
            saturation->setMinimum(1);
            saturation->setMaximum(100);
            saturation->setTickInterval(1);
            saturation->setValue(10);
            connect(saturation, SIGNAL(valueChanged(int)), this, SLOT(saturationChange(int)));
            saturationLable = new QLabel;
            saturationLable->setFrameShape(QFrame::Box);
            saturationLable->setMaximumWidth(28);
            saturationLable->setMinimumWidth(28);

            //QComboBox *brightness;
            //QComboBox *gamma;
            //QComboBox *saturation;
        transferLayout->addWidget(new QLabel(tr("Contrast:")), 0, 0);
        transferLayout->addWidget(contrast, 0, 1);
        transferLayout->addWidget(contrastLable, 0, 2);
        transferLayout->addWidget(new QLabel(tr("Brightness:")), 1, 0);
        transferLayout->addWidget(brightness, 1, 1);
        transferLayout->addWidget(brightnessLable, 1, 2);
        transferLayout->addWidget(new QLabel(tr("Gamma:")), 2, 0);
        transferLayout->addWidget(gamma, 2, 1);
        transferLayout->addWidget(gammaLable, 2, 2);
        transferLayout->addWidget(new QLabel(tr("Saturation:")), 3, 0);
        transferLayout->addWidget(saturation, 3, 1);
        transferLayout->addWidget(saturationLable, 3, 2);

        groupTransfar->setLayout(transferLayout);

        contrastChange(contrast->value());
        brightnessChange(brightness->value());
        gammaChange(gamma->value());
        saturationChange(saturation->value());

    optTransferLayout->addWidget(groupTransfar);
    optTransferLayout->addStretch(1);
    optTransferFrame->setLayout(optTransferLayout);
    }
    QFrame *optEncodersFrame = new QFrame;
    QFrame *optConsoleFrame = new QFrame;{
        QBoxLayout *optConsoleLayout = new QBoxLayout(QBoxLayout::TopToBottom);
        optConsole = new QLineEdit;

        optConsoleLayout->addWidget(new QLabel(tr("Write additional commands:")));
        optConsoleLayout->addWidget(optConsole);
        optConsoleLayout->addStretch(1);
        optConsoleFrame->setLayout(optConsoleLayout);
    }

    optionsTabs->addTab(optQualityFrame, tr("Quality"));
    optionsTabs->addTab(optMetaFrame, tr("Metadata"));
    optionsTabs->addTab(optSuptitlesFrame, tr("Suptitles"));
    //optionsTabs->addTab(optPresetFrame, tr("Preset"));
    optionsTabs->addTab(optSizeFrame, tr("Size"));
    optionsTabs->addTab(optTransferFrame, tr("Transfer"));
    //optionsTabs->addTab(optEncodersFrame, tr("Encoders"));
    optionsTabs->addTab(optConsoleFrame, tr("Console"));}


//Qutput
    QFrame *outputFrame = new QFrame;{
    QBoxLayout *outputLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    outputLayout->setSpacing(0);
    outputLayout->setMargin(2);
        outputPath = new QComboBox;
        outputPath->setToolTip(tr("Output path"));
        outputPath->addItems(*pathsList);
        outputPath->setCurrentIndex(outputPath->count()-1);
        connect(outputPath, SIGNAL(currentIndexChanged(int)), this, SLOT(changedPath()));
        btnOutBrowser = new QPushButton(tr("..."));
        btnOutBrowser->setMaximumWidth(30);
        btnOutBrowser->setToolTip(tr("Select output directory"));
        connect(btnOutBrowser, SIGNAL(clicked()), this, SLOT(setOutputPath()));

     outputLayout->addWidget(new QLabel(tr("Output:")));
     outputLayout->addWidget(outputPath, 1);
     outputLayout->addWidget(btnOutBrowser);

     outputFrame->setLayout(outputLayout);}

//Msg
    QFrame *msgFrame = new QFrame;{
    msgFrame->setMaximumHeight(150);
        QBoxLayout *msgLayout = new QBoxLayout(QBoxLayout::TopToBottom);
        msgLayout->setSpacing(0);
        msgLayout->setMargin(0);
            textMsg = new QTextEdit;
            textMsg->setReadOnly(true);

            progressConvert = new QProgressBar;
            progressConvert->setMinimum(0);
            progressConvert->setMaximum(100);
            progressConvert->setAlignment(Qt::AlignCenter);
            progressConvert->setToolTip(tr("Progress encoding"));

        msgLayout->addWidget(progressConvert);
        msgLayout->addWidget(textMsg);

        msgFrame->setLayout(msgLayout);}



    QSplitter *Splitter = new QSplitter;
    Splitter->addWidget(filesFrame);
    Splitter->addWidget(optionsTabs);

    topLayout->addWidget(Splitter, 1);
    topLayout->addWidget(outputFrame);
    topLayout->addWidget(msgFrame);

    mainFrame->setLayout(topLayout);

    setCentralWidget(mainFrame);
}

void MainWindow::createToolBar(){

    QFrame *toolFrame = new QFrame;
   // toolFrame->setMaximumHeight(32);
    QBoxLayout *toolLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    toolLayout->setSpacing(2);
    toolLayout->setMargin(2);
        openBtn = new QPushButton();
        openBtn->setIcon(QIcon(":/icons/add.png"));
        openBtn->setMaximumSize(32, 32);
        openBtn->setToolTip(tr("Add files"));

        deleteBtn = new QPushButton();
        deleteBtn->setIcon(QIcon(":/icons/delete.png"));
        deleteBtn->setMaximumSize(32, 32);
        deleteBtn->setToolTip(tr("Remove files"));

        infoBtn = new QPushButton();
        infoBtn->setIcon(QIcon(":/icons/information.png"));
        infoBtn->setMaximumSize(32, 32);
        infoBtn->setToolTip(tr("Files info"));

        startBtn = new QPushButton;
        startBtn->setIcon(QIcon(":/icons/go.png"));
        startBtn->setMaximumSize(32, 32);
        startBtn->setToolTip(tr("Start encoding"));

        cancelBtn = new QPushButton;
        cancelBtn->setIcon(QIcon(":/icons/stop.png"));
        cancelBtn->setMaximumSize(32, 32);
        cancelBtn->setToolTip(tr("Cancel encoding"));

        settingBtn = new QPushButton;
        settingBtn->setIcon(QIcon(":/icons/wrench.png"));
        settingBtn->setMaximumSize(32, 32);
        settingBtn->setToolTip(tr("Settings"));

        helpBtn = new QPushButton;
        helpBtn->setIcon(QIcon(":/icons/help.png"));
        helpBtn->setMaximumSize(32, 32);
        helpBtn->setToolTip(tr("Help"));

        toolLayout->addWidget(openBtn);
        toolLayout->addWidget(deleteBtn);
        toolLayout->addWidget(infoBtn);
        toolLayout->addWidget(startBtn);
        toolLayout->addWidget(cancelBtn);
        toolLayout->addStretch(1);
        //toolLayout->addWidget(settingBtn);
        toolLayout->addWidget(helpBtn);

    toolFrame->setLayout(toolLayout);


    mainToolBar = addToolBar("main");

    //mainToolBar->setIconSize(QSize(16, 16));

    mainToolBar->addWidget(toolFrame);

    connect(openBtn, SIGNAL(clicked()), this, SLOT(openFiles()));
    connect(deleteBtn, SIGNAL(clicked()), this, SLOT(deleteFiles()));
    connect(infoBtn, SIGNAL(clicked()), this, SLOT(infoFiles()));
    connect(startBtn, SIGNAL(clicked()), this, SLOT(startConvert()));
    connect(cancelBtn, SIGNAL(clicked()), this, SLOT(cancelConvert()));
    connect(helpBtn, SIGNAL(clicked()), this, SLOT(help()));

}

//Action


//SLOTS
//Files
void MainWindow::checkAllFiles(){
int fcount = filesTable->rowCount();
    if(fcount == 0)return;

int index;
    for(int i=0; i<fcount; ++i){
        index = optionIndex(filesTable->item(i, 1)->data(Qt::UserRole).toString());

        if(filesAllCheck->isChecked()){
            checkhash.value(index)->setChecked(true);
        }else{
            checkhash.value(index)->setChecked(false);
        }
    }
}

void MainWindow::uncheckAllFiles(){
int fcount = filesTable->rowCount();
        if(fcount == 0)return;

int index;
//bool check = false;
int check=0;


        for(int i=0; i<fcount; ++i){
            index = optionIndex(filesTable->item(i, 1)->data(Qt::UserRole).toString());
            if(checkhash.value(index)->isChecked())check++;
        }
        if(check == fcount)filesAllCheck->setChecked(true);
        else filesAllCheck->setChecked(false);

}

//Quality
void MainWindow::novideoCheck(){
    if(novideo->isChecked()){
        vqType->setEnabled(false);
        vqQuValue->setEnabled(false);
        vqBrValue->setEnabled(false);
        softtarget->setEnabled(false);
        speedlevel[0]->setEnabled(false);
        speedlevel[1]->setEnabled(false);
        speedlevel[2]->setEnabled(false);
    }else{
        vqType->setEnabled(true);
        vqQuValue->setEnabled(true);
        vqBrValue->setEnabled(true);
        if(vqType->currentIndex() == 1)softtarget->setEnabled(true);
        speedlevel[0]->setEnabled(true);
        speedlevel[1]->setEnabled(true);
        speedlevel[2]->setEnabled(true);
    }
}

void MainWindow::vqTypeChange(){

    if(vqType->currentIndex() == 0){
        vqDemLabel->setText(tr("Quality:"));
        vqBrValue->setHidden(true);
        vqQuValue->setHidden(false);
        vqValLable->setMaximumWidth(20);
        vqValLable->setMinimumWidth(20);
        softtarget->setEnabled(false);
        vqValLable->setText(QString::number(vqQuValue->value()));
    }else{
        vqDemLabel->setText(tr("Bitrate:"));
        vqQuValue->setHidden(true);
        vqBrValue->setHidden(false);
        vqValLable->setMaximumWidth(32);
        vqValLable->setMinimumWidth(32);
        softtarget->setEnabled(true);
        vqValLable->setText(QString::number(vqBrValue->value()));
    }
}

void MainWindow::vqValueChange(){
    if(vqType->currentIndex() == 0)vqValLable->setText(QString::number(vqQuValue->value()));
    else vqValLable->setText(QString::number(vqBrValue->value()));
}

void MainWindow::noaudioCheck(){
    if(noaudio->isChecked()){
        aqType->setEnabled(false);
        aqQuValue->setEnabled(false);
        aqBrValue->setEnabled(false);
        aqChNumMode[0]->setEnabled(false);
        aqChNumMode[1]->setEnabled(false);
        aqChNumbers->setEnabled(false);
        aqSamplerateMode[0]->setEnabled(false);
        aqSamplerateMode[1]->setEnabled(false);
        aqSamplerate->setEnabled(false);

    }else{
        aqType->setEnabled(true);
        aqQuValue->setEnabled(true);
        aqBrValue->setEnabled(true);
        aqChNumMode[0]->setEnabled(true);
        aqChNumMode[1]->setEnabled(true);
        if(aqChNumMode[1]->isChecked())aqChNumbers->setEnabled(true);
        aqSamplerateMode[0]->setEnabled(true);
        aqSamplerateMode[1]->setEnabled(true);
        if(aqSamplerateMode[1]->isChecked())aqSamplerate->setEnabled(true);
    }
}

void MainWindow::aqTypeChange(){
    if(aqType->currentIndex() == 0){
        aqDemLabel->setText(tr("Quality:"));
        aqBrValue->setHidden(true);
        aqQuValue->setHidden(false);
        aqValLable->setMaximumWidth(20);
        aqValLable->setMinimumWidth(20);
        aqValLable->setText(QString::number(aqQuValue->value()));
    }else{
        aqDemLabel->setText(tr("Bitrate:"));
        aqQuValue->setHidden(true);
        aqBrValue->setHidden(false);
        aqValLable->setMaximumWidth(26);
        aqValLable->setMinimumWidth(26);
        aqValLable->setText(QString::number(aqBrValue->value()));
    }
}

void MainWindow::aqValueChange(){
    if(aqType->currentIndex() == 0)aqValLable->setText(QString::number(aqQuValue->value()));
    else aqValLable->setText(QString::number(aqBrValue->value()));
}

void MainWindow::aqChNumModeChange(){
    if(aqChNumMode[0]->isChecked())aqChNumbers->setEnabled(false);
    else aqChNumbers->setEnabled(true);
}

void MainWindow::aqSrModeChange(){
    if(aqSamplerateMode[0]->isChecked())aqSamplerate->setEnabled(false);
    else aqSamplerate->setEnabled(true);
}

//Size
void MainWindow::sizeChange(){
    if(sizeVideo[0]->isChecked()){
        widthEdit->setEnabled(false);
        heightEdit->setEnabled(false);
        noupscaling->setEnabled(false);
    }else{
        widthEdit->setEnabled(true);
        heightEdit->setEnabled(true);
        noupscaling->setEnabled(true);
    }
}

void MainWindow::aspectChange(){
    if(aspectVideo[0]->isChecked())aspect->setEnabled(false);
    else aspect->setEnabled(true);
}

//Transfer
void MainWindow::contrastChange(int value){
    contrastLable->setText(QString::number(value/10.0));
}

void MainWindow::brightnessChange(int value){
    if(value == 0){
        brightnessLable->setText("0");
        return;
    }
    brightnessLable->setText(QString::number(value/10.0));
}

void MainWindow::gammaChange(int value){
    gammaLable->setText(QString::number(value/10.0));
}

void MainWindow::saturationChange(int value){
    saturationLable->setText(QString::number(value/10.0));
}
//----------------------------------------------
void MainWindow::openFiles(){
QStringList flist;
    flist = QFileDialog::getOpenFileNames(this, tr("Open files"), "", "");

int fcount = flist.count();
flist.sort();

int lastrow = filesTable->rowCount();
//filesTable->insertRow(fcount);
QString fpath;
QString fname;
QCheckBox *chbox;
QTableWidgetItem *item;
    for(int i=0; i<fcount; ++i){
        fpath = flist.at(i);
        fname = fpath.right(fpath.count() - fpath.lastIndexOf("/") - 1);

        filesTable->insertRow(lastrow+i);
        findex++;
        item = new QTableWidgetItem;
        item->setText(fname);
        item->setToolTip(fpath);
        //item->setIcon(QIcon(":/icons/cancel.png"));
        item->setData(Qt::UserRole, QString::number(findex) + "|" + fpath + "|" + "n");
        chbox = new QCheckBox;
        chbox->setToolTip(tr("Check for encoding"));
        connect(chbox, SIGNAL(clicked()), this, SLOT(uncheckAllFiles()));
        checkhash.insert(findex, chbox);
        QBoxLayout *chboxLayout = new QBoxLayout(QBoxLayout::LeftToRight);
        chboxLayout->setSpacing(0);
        chboxLayout->setMargin(4);
        QFrame *chboxFrame = new QFrame;
        chboxLayout->addWidget(chbox);
        chboxFrame->setLayout(chboxLayout);
        filesTable->setCellWidget(lastrow+i, 0, chboxFrame);
        filesTable->setItem(lastrow+i, 1, item);
    }

}

void MainWindow::deleteFiles(){
QList<QTableWidgetItem *> selectItems;

    selectItems = filesTable->selectedItems();

int row=0;
    for(int i=0; i<selectItems.count(); ++i){
        row = selectItems.at(i)->row();
        delete selectItems.at(i);
        filesTable->removeRow(row);
    }
}

void MainWindow::infoFiles(){
QStringList arg;
QList<QTableWidgetItem *> selectItems;
    infoMsgBox->setText("");
    selectItems = filesTable->selectedItems();

    if(selectItems.count() == 0)return;

    for(int i=0; i<selectItems.count(); ++i){
        arg << "--info";
        arg << optionPath(selectItems.at(i)->data(Qt::UserRole).toString());
        coderinfo->start("ffmpeg2theora.exe", arg);
        coderinfo->waitForFinished(9999999);
    }

}

void MainWindow::help(){
    helpBrowser->show();
}

void MainWindow::setOutputPath(){

    outputDir = QFileDialog::getExistingDirectory(this, tr("Select output directory"), "");

    if(outputDir != ""){
        pathsList->append(outputDir);
        outputPath->addItem(outputDir);
        outputPath->setCurrentIndex(outputPath->count()-1);
    }

}

void MainWindow::changedPath(){
    pathsList->clear();

    for(int i=0; i<outputPath->count(); ++i){
        if(i != outputPath->currentIndex())pathsList->append(outputPath->itemText(i));
    }
    pathsList->append(outputPath->currentText());
}

QStringList MainWindow::initArguments(){
QStringList arguments;


    //Output


    //Video quality
        if(novideo->isChecked()){
            arguments << "--novideo";
        }else{
            if(vqType->currentIndex() == 0){
                arguments << "-v" << QString::number(vqQuValue->value());
            }else{
                arguments << "-V" << QString::number(vqBrValue->value());
                if(softtarget->isChecked())arguments << "--soft-target";
            }

            if(speedlevel[0]->isChecked())arguments << "--speedlevel" << "0";
            if(speedlevel[1]->isChecked())arguments << "--speedlevel" << "1";
            if(speedlevel[2]->isChecked())arguments << "--speedlevel" << "2";
        }

    //Audio quality
        if(noaudio->isChecked()){
            arguments << "--noaudio";
        }else{
            if(aqType->currentIndex() == 0)arguments << "-a" << QString::number(aqQuValue->value());
            else arguments << "-A" << QString::number(aqBrValue->value());

            if(aqChNumMode[1]->isChecked())arguments << "-c" << QString::number(aqChNumbers->value());
            if(aqSamplerateMode[1]->isChecked())arguments << "-H" << QString::number(aqSamplerate->value());
        }

    //Metadata
        if(artist->text() != "")arguments << "--artist" << artist->text();
        if(title->text() != "")arguments << "--title" << title->text();
        if(date->text() != "")arguments << "--date" << date->text();
        if(location->text() != "")arguments << "--location" << location->text();
        if(organization->text() != "")arguments << "--organization" << organization->text();
        if(copyright->text() != "")arguments << "--copyright" << copyright->text();
        if(license->text() != "")arguments << "--license" << license->text();
        if(contact->text() != "")arguments << "--contact" << contact->text();
        if(nometadata->isChecked())arguments << "--nometadata";
        if(nooshash->isChecked())arguments << "--no-oshash";

    //Subtitles
        if(stFilePath->text() != ""){
            arguments << "--subtitles" << stFilePath->text();
            arguments << "--subtitles-encoding" << stEncoding->currentText();
            arguments << "--subtitles-language" << stLang->text();
            arguments << "--subtitles-category" << stCategory->text();
            if(ignoreNonUtf8->isChecked())arguments << "--subtitles-ignore-non-utf8";
            if(noSubtitles->isChecked())arguments << "--nosubtitles";
        }

    //Size
        if(sizeVideo[1]->isChecked()){
            //bool ok = false;
            //int width = widthEdit->text().toInt(&ok);
            //if(ok)arguments << "--width" << QString::number(width);

            arguments << "--width" << widthEdit->text();
            arguments << "--height" << heightEdit->text();
            if(noupscaling->isChecked())arguments << "--no-upscaling";
        }

        if(aspectVideo[1]->isChecked()){
            switch(aspect->currentIndex()){
            case 0:{arguments << "--aspect" << QString::number(double(4.0/3.0));}break;
            case 1:{arguments << "--aspect" << QString::number(double(16.0/9.0));}break;
            case 2:{arguments << "--pixel-aspect" << "1";}break;
            case 3:{arguments << "--pixel-aspect" << QString::number(double(4.0/3.0));}break;
            }
        }

        arguments << "--croptop" << cropTop->text();
        arguments << "--cropbottom" << cropBottom->text();
        arguments << "--cropleft" << cropLeft->text();
        arguments << "--cropright" << cropRight->text();

    //Transfer

        arguments << "--contrast" << QString::number((contrast->value())/10.0);
        if(brightness->value() == 0)arguments << "--brightness" << "0";
        else arguments << "--brightness" << QString::number(brightness->value()/10.0);
        arguments << "--gamma" << QString::number(gamma->value()/10.0);
        arguments << "--saturation" << QString::number(saturation->value()/10.0);



    //Console
        arguments << optConsole->text().split(" ");

        return arguments;
}

void MainWindow::startConvert(){

int fcount = filesTable->rowCount();
    if(fcount == 0)return;

QString path = outputPath->currentText();
if(!QDir(path).exists() || path == ""){
    QMessageBox::critical(this, tr("GUI ffmpeg2theora"), tr("Output folder does not exist"));
    return;
}

QStringList arguments = initArguments();
QString finput;
QString options;

bool isFileToConvert = false;
    for(int i=0; i<fcount; ++i){
        options = filesTable->item(i, 1)->data(Qt::UserRole).toString();
        if(checkhash.value(optionIndex(options))->isChecked() && optionConvert(options) == "n"){
            isFileToConvert = true;
            lastConvertedFile = optionIndex(options);
            filesTable->item(i, 1)->setIcon(QIcon(":/icons/go.png"));
            break;
        }
    }
    if(!isFileToConvert)return;
    finput = optionPath(options);
    arguments << "-o" << path + setExtension(finput.right(finput.count() - finput.lastIndexOf("/")), ".ogv");
    arguments << finput;
    coderproc->start("ffmpeg2theora.exe", arguments);

    isStartConvert = true;
    progress=0;
    timer->start(1000);
}

void MainWindow::nextConvert(int code){
    if(!isStartConvert)return;

int fcount = filesTable->rowCount();
QString path = outputPath->currentText();
    if(!QDir(path).exists() || path == ""){
        QMessageBox::critical(this, tr("GUI ffmpeg2theora"), tr("Output folder does not exist"));
        return;
    }

QStringList arguments = initArguments();
QString finput;
QString options;
bool isFileToConvert = false;

        for(int i=0; i<fcount; ++i){
            options = filesTable->item(i, 1)->data(Qt::UserRole).toString();
            if(optionIndex(options) == lastConvertedFile){
                if(code == 0){
                    options = setConverted(options, "y");
                    filesTable->item(i, 1)->setData(Qt::UserRole, options);
                    filesTable->item(i, 1)->setIcon(QIcon(":icons/accept.png"));
                }else{
                    filesTable->item(i, 1)->setIcon(QIcon(""));
                    timer->stop();
                    progressConvert->setValue(0);
                    return;
                }
            }
            if(checkhash.value(optionIndex(options))->isChecked() && optionConvert(options) == "n"){               
                isFileToConvert = true;
                lastConvertedFile = optionIndex(options);
                filesTable->item(i, 1)->setIcon(QIcon(":/icons/go.png"));
                break;
            }
        }
        if(!isFileToConvert){
            timer->stop();
            progressConvert->setValue(100);
            return;
        }
        finput = optionPath(options);
        arguments << "-o" << path + setExtension(finput.right(finput.count() - finput.lastIndexOf("/")), ".ogv");
        arguments << finput;
        coderproc->start("ffmpeg2theora.exe", arguments);

}

void MainWindow::cancelConvert(){

    coderproc->kill();
    isStartConvert = false;
    timer->stop();
    progressConvert->setValue(0);
    progress = 0;

 int fcount = filesTable->rowCount();
 QString options;
    for(int i = 0; i<fcount; ++i){
        options = filesTable->item(i, 1)->data(Qt::UserRole).toString();
        if(optionIndex(options) == lastConvertedFile){
            filesTable->item(i, 1)->setIcon(QIcon(""));
        }
    }
}

void MainWindow::ffmpeg2theoraMsg(){

//QByteArray baout = coderproc->readAllStandardOutput();
//QByteArray baerr = coderproc->readAllStandardError();

QByteArray baout = coderproc->readAll();
//QByteArray baout = coderproc->read(99999);


    qDebug() << "SDTOUT = " << baout;
    //qDebug() << "SDTERR = " << baerr;

    textMsg->append(QString(baout.data()));
    //textMsg->append(QString(baerr.data()));
    //textMsg->setText(QString(baout.data()));
    //textMsg->setText(QString(baerr.data()));

    //timer->start(1000);
}

void MainWindow::infoMsg(){
    QByteArray baout = coderinfo->readAllStandardOutput();
    //QByteArray baerr = coderinfo->readAllStandardError();
    qDebug() << "INFO = " << baout;
    QString text = infoMsgBox->toPlainText();
    infoMsgBox->setText(text + baout);
    infoMsgBox->show();
    //qDebug() << "SDTERR2 = " << baerr;
}

void MainWindow::timerAction(){
//static int progress = 0;
    if(progress >= 100)progress = 0;
    progressConvert->setValue(progress++);

}

//Utils
int MainWindow::optionIndex(QString stroptions){
QStringList options = stroptions.split("|");
bool ok = false;
int index = 0;
    if(options.count() >= 1){
        index = options[0].toInt(&ok);
        if(ok) return index;
        else return -1;
    }else return -1;
}

QString MainWindow::optionPath(QString stroptions){
QStringList options = stroptions.split("|");
    if(options.count() >= 2){
        return options[1];
    }else return "-1";
}

QString MainWindow::optionConvert(QString stroptions){
QStringList options = stroptions.split("|");
    if(options.count() >= 3){
        return options[2];
    }else return "-1";
}

QString MainWindow::setConverted(QString stroptions, QString opt){
QStringList options = stroptions.split("|");
QString stropt = "";
    if(options.count() >= 3){
        options[2] = opt;
    }else{
        switch(options.count()){
        case 0: stropt = " | |" + opt;
        case 1: options[0] + "| |" + opt;
        case 2: options[0] + options[1] + "|" + opt;
        }
        return stropt;
    }

    return options.join("|");
}

QString MainWindow::setExtension(QString fname, QString ext){
QString name = fname.left(fname.lastIndexOf("."));

    return name + ext;
}
