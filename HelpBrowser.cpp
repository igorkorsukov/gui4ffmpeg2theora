#include "HelpBrowser.h"

QUHelpBrowser::QUHelpBrowser(QString wtitle, QIcon wicon){

    Qt::WindowFlags flags = 0;
    flags = Qt::Window;

    windowTitle = wtitle;
    this->setWindowFlags(flags);
    this->setWindowTitle(windowTitle);
    this->setWindowIcon(wicon);


    homeButton = new QPushButton;
    homeButton->setIcon(QIcon(":/icons/home.png"));
    homeButton->setMaximumSize(32, 32);
    homeButton->setToolTip(tr("Home"));

    backButton = new QPushButton;
    backButton->setIcon(QIcon(":/icons/back.png"));
    backButton->setMaximumSize(32, 32);
    backButton->setToolTip(tr("Back"));
    backButton->setEnabled(false);

    forwardButton = new QPushButton;
    forwardButton->setIcon(QIcon(":/icons/forward.png"));
    forwardButton->setMaximumSize(32, 32);
    forwardButton->setToolTip(tr("Forward"));
    forwardButton->setEnabled(false);

    printButton = new QPushButton;
    printButton->setIcon(QIcon(":/icons/printer.png"));
    printButton->setMaximumSize(32, 32);
    printButton->setToolTip(tr("Print this page"));
    printButton->setShortcut(QKeySequence("Ctrl+P"));

    textBrowser = new QTextBrowser;
    //textBrowser->setSearchPaths(QStringList() << "docs");
    textBrowser->setSource(QString("qrc:/docs/en_index.html"));
    setWindowTitleFromDoc();

    btnLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    btnLayout->setSpacing(2);
    btnLayout->setMargin(2);
    btnLayout->addWidget(homeButton, 0, Qt::AlignLeft);
    btnLayout->addWidget(backButton, 0, Qt::AlignLeft);
    btnLayout->addWidget(forwardButton, 0, Qt::AlignLeft);
    btnLayout->addStretch(1);
    btnLayout->addWidget(printButton, 0, Qt::AlignRight);

    mainLayout = new QBoxLayout(QBoxLayout::TopToBottom);
    mainLayout->setSpacing(4);
    mainLayout->setMargin(0);
    mainLayout->addLayout(btnLayout);
    mainLayout->addWidget(textBrowser);

    this->setLayout(mainLayout);
    this->resize(600, 440);

    connect(homeButton, SIGNAL(clicked()), textBrowser, SLOT(home()));
    connect(backButton, SIGNAL(clicked()), textBrowser, SLOT(backward()));
    connect(forwardButton, SIGNAL(clicked()), textBrowser, SLOT(forward()));
    connect(textBrowser, SIGNAL(backwardAvailable(bool)), backButton, SLOT(setEnabled(bool)));
    connect(textBrowser, SIGNAL(forwardAvailable(bool)), forwardButton, SLOT(setEnabled(bool)));

    connect(textBrowser, SIGNAL(sourceChanged(QUrl)), this, SLOT(setWindowTitleFromDoc()));

    connect(printButton, SIGNAL(clicked()), this, SLOT(printDialog()));

}

void QUHelpBrowser::printDialog(){
    QPrinter printer;
    QPrintDialog* PrintDialog = new QPrintDialog(&printer);
    if(PrintDialog->exec() == QDialog::Accepted){
        textBrowser->print(&printer);
    }
    delete PrintDialog;
}

void QUHelpBrowser::setWindowTitleFromDoc(){
QString doctitle = textBrowser->documentTitle();
    if(doctitle == "")this->setWindowTitle(windowTitle);
    else this->setWindowTitle(doctitle);
}
