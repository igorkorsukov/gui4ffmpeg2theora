# -------------------------------------------------
# Project created by QtCreator 2010-04-15T10:02:05
# -------------------------------------------------
TARGET = GUI_ffmpeg2theora
TEMPLATE = app
SOURCES += main.cpp \
    MainWindow.cpp \
    HelpBrowser.cpp
HEADERS += MainWindow.h \
    HelpBrowser.h
RESOURCES += resources.qrc
#RC_FILE = icon.rc

OTHER_FILES += \
    !ToDo.txt
