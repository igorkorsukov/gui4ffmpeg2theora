#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore/QDebug>
#include <QtGui/QMainWindow>
#include <QtGui/QToolBar>
#include <QtGui/QPushButton>
#include <QtGui/QTableWidget>
#include <QtGui/QHeaderView>
#include <QtGui/QGridLayout>
#include <QtGui/QFrame>
#include <QtGui/QBoxLayout>
#include <QtGui/QComboBox>
#include <QtGui/QLabel>
#include <QtGui/QCheckBox>
#include <QtCore/QStringList>
#include <QFileDialog>
#include <QtCore/QProcess>
#include <QtGui/QStandardItemModel>
#include <QMessageBox>
#include <QtGui/QTextEdit>
#include <QtGui/QTabWidget>
#include <QtGui/QGroupBox>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QSlider>
#include <QtGui/QSplitter>
#include <QtGui/QRadioButton>
#include <QtGui/QButtonGroup>
#include <QtGui/QSpinBox>
#include <QtCore/QHash>
#include <QtGui/QProgressBar>
#include <QtCore/QTimer>
#include <QtCore/QSettings>

#include "HelpBrowser.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void checkAllFiles();
    void uncheckAllFiles();

    void novideoCheck();
    void vqTypeChange();
    void vqValueChange();

    void noaudioCheck();
    void aqTypeChange();
    void aqValueChange();
    void aqChNumModeChange();
    void aqSrModeChange();

    void sizeChange();
    void aspectChange();

    void contrastChange(int value);
    void brightnessChange(int value);
    void gammaChange(int value);
    void saturationChange(int value);

    void openFiles();
    void deleteFiles();
    void infoFiles();
    void startConvert();
    void nextConvert(int code);
    void cancelConvert();
    void help();
    void setOutputPath();
    void changedPath();

    void ffmpeg2theoraMsg();
    void infoMsg();

    void timerAction();

private:

    void createToolBar();
    void createMainUI();

    QStringList initArguments();
    int optionIndex(QString stroptions);
    QString optionPath(QString stroptions);
    QString optionConvert(QString stroptions);
    QString setConverted(QString stroptions, QString opt);
    QString setExtension(QString fname, QString ext);


    QTimer *timer;
    QUHelpBrowser *helpBrowser;
    QSettings *settings;

    QHash<int, QCheckBox*> checkhash;
    int findex;
    bool isStartConvert;
    int lastConvertedFile;
    int progress;

    QToolBar *mainToolBar;
        QPushButton *openBtn;
        QPushButton *deleteBtn;
        QPushButton *infoBtn;
        QPushButton *startBtn;
        QPushButton *cancelBtn;
        QPushButton *settingBtn;
        QPushButton *helpBtn;

    QTableWidget *filesTable;
    QCheckBox *filesAllCheck;
    QLabel *lblOutput;
    QComboBox *outputPath;
    QStringList *pathsList;
    QStringList *pathKeysList;
    QPushButton *btnOutBrowser;


    QProgressBar *progressConvert;

    QTextEdit *textMsg;

    QTabWidget *optionsTabs;

    QStringList listFiles;
    QString outputDir;

    QStandardItemModel *filesModel;

    QProcess *coderproc;
    QProcess *coderinfo;


    QFrame *infoFrame;
    QTextEdit *infoMsgBox;

//Options
//Video Quality
    QCheckBox *novideo;
    QComboBox *vqType;
    QLabel *vqDemLabel;
    QSlider *vqQuValue;
    QSlider *vqBrValue;
    QLabel *vqValLable;
    QCheckBox *softtarget;
    QRadioButton *speedlevel[3];

//Audio Quality
    QCheckBox *noaudio;
    QComboBox *aqType;
    QLabel *aqDemLabel;
    QSlider *aqQuValue;
    QSlider *aqBrValue;
    QLabel *aqValLable;
    QRadioButton *aqChNumMode[2];
    QSpinBox *aqChNumbers;
    QRadioButton *aqSamplerateMode[2];
    QSpinBox *aqSamplerate;

//Metadata
    QLineEdit *artist;
    QLineEdit *title;
    QLineEdit *date;
    QLineEdit *location;
    QLineEdit *organization;
    QLineEdit *copyright;
    QLineEdit *license;
    QLineEdit *contact;
    QCheckBox *nometadata;
    QCheckBox *nooshash;

//Suptitles
    QLineEdit *stFilePath;
    QComboBox *stEncoding;
    QLineEdit *stLang;
    QLineEdit *stCategory;
    QCheckBox *ignoreNonUtf8;
    QCheckBox *noSubtitles;

//Size
    QRadioButton *sizeVideo[2];
    QLineEdit *widthEdit;
    QLineEdit *heightEdit;
    QCheckBox *preserveAspect;
    QCheckBox *noupscaling;

    QRadioButton *aspectVideo[2];
    QComboBox *aspect;

    QLineEdit *cropTop;
    QLineEdit *cropBottom;
    QLineEdit *cropLeft;
    QLineEdit *cropRight;

//Transfer
    QSlider *contrast;
    QSlider *brightness;
    QSlider *gamma;
    QSlider *saturation;

    QLabel *contrastLable;
    QLabel *brightnessLable;
    QLabel *gammaLable;
    QLabel *saturationLable;

//Console
    QLineEdit *optConsole;


};

#endif // MAINWINDOW_H
